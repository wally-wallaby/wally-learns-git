# Workshop prerequisites

This workshop requires the following software and user account setup:

- Sublime text (or your favorite text editor)
- Git version 2.12 or later
- Bitbucket account and SSH key set up

## Install Sublime Text 3

This workshop will use Sublime Text 3 for demonstrations, but you are welcome to 
use any text editor.

1. Download and install Sublime Text 3: https://www.sublimetext.com/3

** Enable spellcheck for Markdown files.**

1. Open a Markdown file (file ending in .md). 
1. Go to **Sublime Text** main menu > **Preferences** > **Settings - Syntax Specific**.
1. Add `"spell_check": true` between the { and }. For example:

```
// These settings override both User and Default settings for the Markdown syntax

{
   "spell_check": true
}

```

## Install Git

Before following the Git setup instructions, check to see if you already have 
a version of Git installed. 

- Open Terminal, and run the following command: 
    ```
    git --version
    git version 2.7.0 (Apple Git-66)
    ```

If you see a version of Apple Git or no version of Git returned, complete the 
instructions below. (Apple Git is a fork of Git that is maintained by Apple and 
may be running behind in features.)

**Install Git using Homebrew**

If you are using Mac, install [Homebrew](https://brew.sh/) to manage your Git 
installation. Homebrew is a package manager that makes installing and updating 
various software easier:

- In Terminal, and run the following command:

```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

Use Homebrew to install Git:

1. In Terminal, run the following command:

```
brew install git
```

1. Verify Git installed and is up-to-date (version 2.14.1 is the latest at the time of writing):

```
git --version
git version 2.14.1
```

**Troubleshooting**

If the Git version doesn't match, then you may have an issue with the PATH referencing the wrong version. To fix this, you'll need to point your PATH to the appropriate installation. 

Fully documenting this fix is outside the scope of these installation instructions, however, there are a number of resources on Stack Overflow, such as [How to find the install path of git in Mac or Linux?](https://stackoverflow.com/questions/31128783/how-to-find-the-install-path-of-git-in-mac-or-linux) that may be helpful.

Alternatively, we can help you fix this before the workshop starts. Just let us know!

## Set your identity

After installing Git, you need to set up your identity. Setting your identity is important because Git uses your name and email address for each commit you make. 

1. In Terminal, run the following command, replacing *First Last* with your first and last name (the quotes are required):

```
git config --global user.name "First Last"
```

2. In Terminal, run the following command, replacing *your-email@example.com* with your email:

```
git config --global user.email your-email@example.com
```

This is the only user configuration required for the workshop, but you may also choose to set other options. For more detail on user configuration, see [Getting Started - First Time Git Setup](https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup).

## Create a Bitbucket account

Sign up for a free Bitbucket account: [https://bitbucket.org/account/signup/](https://bitbucket.org/account/signup/)

## Configure your SSH key

SSH is a secure way to communicate with Bitbucket without entering your username 
and password each time. Complete the setup in the [Set up SSH for Git](https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html) 
guide. This setup requires a little patience and some trial and error.

If you get really stuck, come to the meetup a few minutes early and ask for help.