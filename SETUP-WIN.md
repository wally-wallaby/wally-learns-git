# Workshop prerequisites

This workshop requires the following software and user account setup:

- Sublime text (or your favorite text editor)
- Git version 2.12 or later
- Bitbucket account and SSH key set up

## Install Sublime Text 3

This workshop will use Sublime Text 3 for demonstrations, but you are welcome to 
use any text editor.

1. Download and install Sublime Text 3: https://www.sublimetext.com/3

** Enable spellcheck for Markdown files.**

1. Open a Markdown file (file ending in .md). 
1. Go to **Sublime Text** main menu > **Preferences** > **Settings - Syntax Specific**.
1. Add `"spell_check": true` between the { and }. For example:

```
// These settings override both User and Default settings for the Markdown syntax

{
   "spell_check": true
}
```

## Install Git

These instructions were written using Windows 10. The steps are similar on other 
versions.

1. Download the appropriate package from http://git-scm.com/download/win 
1. Run `Git-2.14.1-64-bit.exe`, and step through the installer, accepting the default settings.
1. Open Git Bash and verify the installation by running the following command:
    
```
git --version
git version 2.14.1.windows.1
```

*Note: If you have trouble starting and using Git Bash, then you may need to restart your computer.*

## Set your identity

After installing Git, you need to set up your identity. Setting your identity 
is important because Git uses your name and email address for each commit you make. 

1. In Git Bash, run the following command, replacing *First Last* with your 
first and last name (the quotes are required):

```
git config --global user.name "First Last"
```

2. In Git Bash, run the following command, replacing *your-email@example.com* with your email:

```
git config --global user.email your-email@example.com
```

This is the only user configuration required for the workshop, but you may also 
choose to set other options. For more detail on user configuration, see 
[Getting Started - First Time Git Setup](https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup).

## Create a Bitbucket account

Sign up for a free Bitbucket account: [https://bitbucket.org/account/signup/](https://bitbucket.org/account/signup/)

## Configure your SSH key

SSH is a secure way to communicate with Bitbucket without entering your username 
and password each time.

If you accepted the default settings when installing Git Bash (bundled with Git), 
then you will have an SSH client installed and are ready to complete the setup 
in the [Set up SSH for Git](https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html) 
guide. This setup requires a little patience and some trial and error.

If you get really stuck, come to the meetup a few minutes early and ask for help.