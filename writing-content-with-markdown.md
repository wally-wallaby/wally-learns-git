---
title: "Writing content with Markdown"
date: 08-03-2017
---

# Writing content with Markdown

Markdown is a simple, lightweight markup language that has become very popular 
for creating content, especially for crowdsourced projects. Markdown comes in 
several flavors, including [CommonMark](http://commonmark.org/) and 
[GitHub-flavored Markdown](https://guides.github.com/features/mastering-markdown/).

## Formatting with Markdown

Markdown was designed by John Gruber as a lightweight alternative to HTML. It 
supports a number of HTML block elements and inline formatting. Below is a list 
of some of the most commonly used:

- Block elements: headings, paragraphs, code blocks, block quotes, horizonal rules
- Inline formatting: strong, emphasis, code (monospace font), images

For a more complete list and original syntax, see 
[Markdown: Syntax](https://daringfireball.net/projects/markdown/syntax) or a 
style sheet for the flavor that you're using.

## Other common lightweight markup languages

You may also encounter content written in other lightweight markup languages, 
including ASciiDoc and reStructuredText.

Many of these languages include additional syntax for classes, tables, and other 
elements that are lacking in Markdown. See the 
[Comparison of features chart](https://en.wikipedia.org/wiki/Lightweight_markup_language#Comparison_of_language_features) 
on Wikipedia for a list of features by language.