# Git workshop activities

All of the activities will be done together in class. I'll explain each one as we go through, but if you're like me, you love reading ahead, so I've included a bit of context and a few tips for each activity.

Helpful commands:

- git branch  // lists the branches and indicates which one you're using
- git log  // displays the commit history for the current branch
- git status  // displays the working tree status, including added, deleted, and modified files as well as indicating which changes are staged 

Each of the lines above has a // after the command to indicate the beginning of a comment. This convention is used throughout this document. Do not include the // in any commands you type into the command line.

## Activity 1: git a little help

Git documentation is accessible from the command line. This comes in handy when you're trying to remember how to use a command or when you can't quite remember which arguments to use.

Try it for yourself

git --help

# Activity 2: clone the repository

Cloning a repository (repo) is the act of making a copy of the entire repository on your computer. Another way of thinking about this is that you're downloading your own copy of whatever is stored in Bitbucket or GitHub (the remote). 

In addition to the contents of the repository, you also get the entire history of changes that have been made. These are accessible with the git log command, which we'll talk about later.

Try it yourself

git clone git@bitbucket.org:toddbecky/learn-git.git
cd learn-git

Tips: 

- If you see an error that mentions anything with the word key or permission denied in it, then it is possible that you may not have configured your SSH key properly. Ask someone for help.
- If you are prompted for a username and password, then you may have copied the HTTPS option. This is OK, but you will need to enter your username and password frequently.

Activity 3: create a branch

When you downloaded the repository, you automatically checked out the main branch. The learn-git repository's main branch is master. You can see which branch you're on by using the git branch command.

Try it yourself

git branch

As a general rule of thumb, you want to create a branch for any work that you do. This allows you to keep your work isolated from the work of others and makes it easier to review later. There are two ways to create a branch. We'll use a built in short cut that creates a branch and then checks it out:

Try it yourself (replace fml with your first, middle, and last initials)

git checkout -b fml-first-branch

Tip: 

The above command is actually a shorthand version of two commands: 

- git branch <branch-name>  // creates a new branch
- git checkout <branch-name>  // checks out the specified branch

# Activity 4: let's do work

Now it is time to do something fun! Let's edit a document written in Markdown.

1. Create a copy of about-berries.md
1. Save the document using the following convention: fml-about-berries.md
1. Edit the document. There are a number of spelling errors and typos.

# Activity 5: add, commit, push

Remember how we created a local clone of the remote repository in stored Bitbucket? Now it is time to prepare a commit and push your changes This process involves three steps, each with different commands:

1. git add <file> // stages the files that you want to share
2. git commit  // makes a record of your added changes with a message about what you did


The git commit command requires you to add a message about what you changed. You may do this in one of two ways: 

- make an inline message using the -m argument (e.g., git commit -m "My first commit")
- omit the -m argument and use your designated text editor to complete the message

Regardless of which you choose, write a brief and descriptive message about what you changed. Remember this message will be read by others and visible in the history. 

Try it yourself

git add fml-about-berries.md
git commit -m "Corrected spelling errors and typos"

Tips:

- Did you add something by mistake? No sweat. Use git reset <file> to remove it from the current index (another way of saying "added files") without changing anything else.
- Use git add . to add all modified and new files in the current working directory, but be very careful using this command.
- The last commit message may be edited by using the git commit --amend command. However, it is generally a good idea to do this only before you push your changes. Never amend public commits.

# Activity 6: push your work

Now that we've created a commit, it is time to push it to Bitbucket. The git push command sends a copy of all non-pushed commits to Bitbucket (the remote repository). 

Specify the name of the remote and branch as arguments in the command. In this case, our remote is called origin (this is the default name), and the branch is the one you created in Activity 2.

Try it yourself

git push origin fml-first-branch

Tip:

- Keep in mind that you can repeat the add, commit, and push workflow as many times as needed before you create a pull request for others to review.

# Activity 7: create a pull request

Once you are ready, we'll walk through the process of creating a pull request together as a group.

Tips for creating pull requests:

- Use a good title and a brief but thorough description of the changes you made.
- Indicate what you want the reviewers to comment on and review (e.g., please check grammar and technical accuracy). Format this list into a checklist if possible.
- Select reviewers who are responsible for approving and merging changes. Depending on the project and team, this may include product owners, developers, quality assurance, or other technical writers. You may also find that default reviewers are assigned.

# Activity 7: keep in sync

There are two commands that you can use to update your local repository:

- git fetch  // downloads changes on the remote repository without changing anything in your local repository; defaults to origin if no remote is specified
- git pull <remote> <branch> // downloads changes (git fetch) and then applies them to your local branch using git merge FETCH_HEAD

Try it yourself

git checkout master
git fetch
git pull origin master

# Bonus activity: resolve a merge conflict

We'll walk through this together if there is time left in the workshop.