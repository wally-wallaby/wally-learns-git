# README #

This repository is a sandbox for learning Git. Setup instructions 
can be found in the following files:

- [macOS setup](SETUP-MAC.md)
- [Windows OS setup](SETUP-WIN.md)

## Contribution guidelines ##

Contributions are welcome and encouraged as a part of the learning Git 
workshop. To make a contribution, simply create a pull request.

## License ##

Copyright (c) 2017. Apache 2.0 licensed. See [LICENSE.txt](LICENSE.txt).


## Contact information ##

Contact [Becky Todd](https://bitbucket.org/toddbecky) with questions or 
comments.